# Confluence Data Export

## How to recreate:

1. Export Confluence space
   1. Navigate to space
   1. Select `Space settings`
   1. Select `Export space`
   1. Select `HTML` for export type
1. Upload contents to a GitLab project
1. Add a `.gitlab-ci.yml` file with template Pages HTML